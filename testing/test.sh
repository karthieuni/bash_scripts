#!/bin/sh

#chmod +x install.sh first
#for creating log file run as this:
#bash -x install.sh 2>&1 | tee installationlog.txt

echo ________________________________________
echo Starting upgrades and installs
apt-get update
apt-get upgrade -y
apt-get install python3-pip ssl-cert python git-core curl python-dev minicom sudo screen nginx -y
echo Upgrades and installs finished
echo ________________________________________


echo ________________________________________
echo Setting up networking settings:
echo interface\ eth0 >> /etc/dhcpcd.conf
echo static\ ip_address=192.168.10.2 >> /etc/dhcpcd.conf   #ADD YOUR OWN IP HERE
echo Static IP set to 192.168.10.2
echo static\ routers=192.168.10.1 >> /etc/dhcpcd.conf      #ADD YOUR OWN GATEWAY HERE
echo Gateway set to 192.168.10.1
echo Port\ 1100 >> /etc/ssh/sshd_config
echo listening ssh port changed to 1100                    #ADD YOUR OWN PORT HERE
echo Networking settings finished setting up.
echo ________________________________________

echo ________________________________________
echo Enabling UART connection:
echo enable_uart=1 >> /boot/config.txt
echo UART connection enabled
echo ________________________________________

echo ________________________________________
echo Fetching SSH public keys from Gitlab
mkdir ~/.ssh
curl https://gitlab.com/jambove.keys >> ~/.ssh/authorized_keys  ##ADD YOUR OWN GITLAB USERS HERE
curl https://gitlab.com/ak98.keys >> ~/.ssh/authorized_keys     ##ADD YOUR OWN GITLAB USERS HERE
curl https://gitlab.com/nick3359.keys >> ~/.ssh/authorized_keys ##ADD YOUR OWN GITLAB USERS HERE
echo SSH keys set
sudo systemctl enable ssh
sudo systemctl start ssh
echo SSH enabled
echo ________________________________________

echo ________________________________________
echo Control phase:
cat /etc/dhcpcd.conf | grep ip_address
cat /etc/dhcpcd.conf | grep routers
cat /etc/ssh/sshd_config | grep Port
cat ~/.ssh/authorized_keys
cat /boot/config.txt | grep uart
systemctl status ssh
echo ________________________________________

echo Installation has finished.

#!/bin/bash
#Reinstallation Script
#First allow permissions to user: sudo chmod u+x reinstall.sh
#To run the automated script: sudo ./reinstall.sh

#Details
project_link="https://gitlab.com/thaiswnielsen/itt2_project_officium/-/archive/master/itt2_project_officium-master.zip"
gitrepourl="git@gitlab.com:thaiswnielsen/itt2_project_officium.git"
project_name="itt2_project_officium"
flask_api_path="/Flask_api/menu.py"
user1=karthieuni
user2=thaiswnielsen
user3=Henry1234
ip_address=8.8.8.8
DNS=google.com
#Networking Settings
Interface="eth0"
Ip="10.10.1.2/24"
Router_ip="10.10.1.1"
DNS_server="8.8.8.8"

#Ping Test
echo -e "\e[4mPing Test - 8.8.8.8, DNS\e[0m"
sudo ping -c 3 $ip_address
echo ______________________________________________
sudo ping -c 3 $DNS
echo ______________________________________________
echo "Internet Connection Is Established"
echo ______________________________________________
#Install packages
echo -e "\e[4mChecking for Updates & Upgrades\e[0m"
sudo apt update && sudo apt upgrade -y
echo -e "\e[4mInstalling python3-pip\e[0m"
sudo apt-get -y -q install python3-pip
echo -e "\e[4mInstalling unzip\e[0m"
sudo apt-get install unzip
echo -e "\e[4mInstalling pyserial\e[0m"
sudo pip3 install pyserial -q
echo -e "\e[4mInstalling flask\e[0m"
pip3 install flask -q
pip3 install flask_restful -q
pip3 install flask_cors -q
echo ______________________________________________
#Unzip project from a zip file
echo -e "\e[4mFetching and downloading Project\e[0m"
echo This will take around 1 minute to download and unzip:
wget -O $project_name.zip $project_link
unzip file
unzip -o $project_name.zip
cp -a itt2_project_officium-master itt2_project_officium
sudo rm -rf itt2_project_officium-master
sudo rm -rf itt2_project_officium-master.zip
pwd
echo _______________________________________________
#Clone repository
#echo -e "\e[4mAdd SSH-keys to Gitlab to clone the repo\e[0m"
#echo "Cloning git repository"
#sudo git clone $gitrepourl
#echo ______________________________________________
#Add network config for static IP
#echo -e "\e[4mConfiguring Static IP\e[0m"
#echo "" >> /etc/dhcpcd.conf
#echo "interface $Interface" >> /etc/dhcpcd.conf
#echo "static ip_address=$Ip" >> /etc/dhcpcd.conf
#echo "static routers=$Router_ip" >> /etc/dhcpcd.conf
#echo "static domain_name_servers=$DNS_server" >> /etc/dhcpcd.conf
#cat /etc/dhcpcd.conf
echo _______________________________________________
#Enabling SSH and VNC server
echo -e "\e[4mEnabling SSH and VNC server\e[0m"
sudo systemctl enable ssh
sudo systemctl start ssh
sudo systemctl start vncserver-x11-serviced.service
sudo systemctl enable vncserver-x11-serviced.service
echo _______________________________________________
#Add SSH-Keys to Authorized Keys
echo -e "\e[4mAdd SSH-Keys to Authorized Keys\e[0m"
#sudo mkdir /home/pi/.ssh
#touch /home/pi/.ssh/authorized_keys
echo "" > /home/pi/.ssh/authorized_keys
sudo curl https://gitlab.com/$user1.keys >> ~/.ssh/authorized_keys
#sudo curl https://gitlab.com/$user2.keys >> ~/.ssh/authorized_keys
#sudo curl https://gitlab.com/$user3.keys >> ~/.ssh/authorized_keys
cat ~/.ssh/authorized_keys
echo ______________________________________________
#Enables UART ttys0
echo -e "\e[4mEnabling UART\e[0m"
echo "" >> /boot/config.txt
echo "enable_uart=1" >> /boot/config.txt
cat /boot/config.txt
#Disables UART ttys0
#echo -e "\e[4mDisabling UART\e[0m"
#echo "enable_uart=1" | sudo tee -a /boot/config.txt >> /dev/null
#sudo systemctl stop serial-getty@ttyS0.service
#sudo systemctl disable serial-getty@ttyS0.service
#sudo sed -e s/console=serial0,115200//g -i.backup /boot/cmdline.txt
echo ______________________________________________
#Auto-run Flask
echo -e "\e[4mAuto-run Flask\e[0m"
echo "[Unit]" > /lib/systemd/system/flask_api.service
echo "Description=Flask api service" >> /lib/systemd/system/flask_api.service
echo "[Service]" >> /lib/systemd/system/flask_api.service
echo "Type=static" >> /lib/systemd/system/flask_api.service
echo "ExecStart=/usr/bin/python3 /home/pi/"$project_name$flask_api_path >> /lib/systemd/system/flask_api.service
echo "[Install]" >> /lib/systemd/system/flask_api.service
echo "WantedBy=boot-complete.target" >> /lib/systemd/system/flask_api.service
sudo chmod 644 /lib/systemd/system/flask_api.service
sudo systemctl daemon-reload
sudo systemctl enable flask_api.service
cat /lib/systemd/system/flask_api.service
echo ______________________________________________
echo "Installation Complete"
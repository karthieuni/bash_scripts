#!/bin/bash

#Have access to webserver: ssh -i .ssh/..s.pem ubuntu@IP_ADDR -p PORT_NR
#First allow permissions to user: sudo chmod u+x cowrie1.sh
#To run the automated script: sudo ./cowrie1.sh
#Only tested on ubuntu 18.04
#Remember that in Step 2 you need to agree with "YES"
#https://nxnjz.net/2019/01/deploying-an-interactive-ssh-honeypot-on-ubuntu-18-04/

echo ________________________________________
echo 1. ONLY ONE TIME FAIL - Create a pass for the users root and cowrie
echo ________________________________________
sudo passwd root
echo __________ ROOT user created____________
sudo adduser --disabled-password cowrie
sudo passwd cowrie
echo __________ COWRIE user created__________

echo ________________________________________
echo 2. Starting updates and upgrades
echo ________________________________________
sudo apt update && sudo apt upgrade -y
echo ________________________________________

echo 3. Installing dependencies and required packages
echo ________________________________________
sudo apt install -y iptables iptables-persistent linux-libc-dev make virtualenv python-virtualenv libfakeroot libssl-dev libffi-dev build-essential libpython3-dev python3-minimal authbind git 
echo ________________________________________

echo 4. Install pip
echo ________________________________________
sudo apt install python-pip -y
echo ________________________________________

echo "First create virtualenv and run cowrie2.sh"

#5. Manual Operation - Type the commands to create virtualenv
#________________________________________
#su - cowrie
#cd /home/cowrie
#git clone http://github.com/cowrie/cowrie
#cd cowrie/
#virtualenv --python=/usr/bin/python3 cowrie-env
#--------NOW ACTIVATING VIRTUALENV----------
#. cowrie-env/bin/activate
#pip install --upgrade pip
#pip install --upgrade -r requirements.txt
#bin/cowrie start
#bin/cowrie stop
#exit

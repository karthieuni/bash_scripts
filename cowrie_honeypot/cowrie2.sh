#!/bin/bash

#Have access to webserver: ssh -i .ssh/..s.pem ubuntu@IP_ADDR -p PORT_NR
#First allow permissions to user: sudo chmod u+x cowrie2.sh
#To run the automated script: sudo ./cowrie2.sh
#Only tested on ubuntu 18.04
#https://nxnjz.net/2019/01/deploying-an-interactive-ssh-honeypot-on-ubuntu-18-04/

echo 6. Added configparser ... .cfg file
echo ________________________________________
sudo cp -a /home/cowrie/cowrie/etc/cowrie.cfg.dist /home/cowrie/cowrie/etc/cowrie.cfg
grep 'hostname = svr04' /home/cowrie/cowrie/etc/cowrie.cfg
sed -i 's/hostname = svr04/hostname = testserver5/g' /home/cowrie/cowrie/etc/cowrie.cfg
grep 'hostname = testserver5' /home/cowrie/cowrie/etc/cowrie.cfg

echo 7. Replace string in Cowrie file
echo ________________________________________
cd /home/cowrie/cowrie/bin/
grep 'DAEMONIZE=""' cowrie
sed -i 's/DAEMONIZE=""/DAEMONIZE="-n"/g' cowrie
grep 'DAEMONIZE="-n"' cowrie
echo ________________________________________

echo 8. Create a new Systemd unit file
echo ________________________________________
touch /etc/systemd/system/cowrie-honeypot.service
echo "[Unit]" >> /etc/systemd/system/cowrie-honeypot.service
echo "Description=Interactive SSH Honeypot" >> /etc/systemd/system/cowrie-honeypot.service
echo "Wants=network.target" >> /etc/systemd/system/cowrie-honeypot.service
echo "[Service]" >> /etc/systemd/system/cowrie-honeypot.service
echo "Type=simple" >> /etc/systemd/system/cowrie-honeypot.service
echo "User=cowrie" >> /etc/systemd/system/cowrie-honeypot.service
echo "Group=cowrie" >> /etc/systemd/system/cowrie-honeypot.service
echo "ExecStart=/home/cowrie/cowrie/bin/cowrie start" >> /etc/systemd/system/cowrie-honeypot.service
echo "Restart=on-failure" >> /etc/systemd/system/cowrie-honeypot.service
echo "RestartSec=5" >> /etc/systemd/system/cowrie-honeypot.service
echo "[Install]" >> /etc/systemd/system/cowrie-honeypot.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/cowrie-honeypot.service
cat /etc/systemd/system/cowrie-honeypot.service
echo ________________________________________

echo 9. Cowrie Status Checks
echo ________________________________________
sudo systemctl daemon-reload
sudo systemctl start cowrie-honeypot.service
sudo systemctl enable cowrie-honeypot.service
echo Done
echo ________________________________________

echo 10. Replace string in /etc/sysctl.conf
echo ________________________________________
grep '#net.ipv4.ip_forward=1' /etc/sysctl.conf
sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
grep 'net.ipv4.ip_forward=1' /etc/sysctl.conf
echo ________________________________________

echo 11. Adding to IPtables
echo ________________________________________
sudo iptables -A PREROUTING -t nat -p tcp --dport 22 -j REDIRECT --to-port 2222
sudo iptables-save > /etc/iptables/rules.v4
cat /etc/iptables/rules.v4
echo Done
echo ________________________________________

echo 12. Add file userdb.txt
echo ________________________________________
touch /home/cowrie/cowrie/etc/userdb.txt
echo "root:x:!root" >> /home/cowrie/cowrie/etc/userdb.txt
echo "root:x:!123456" >> /home/cowrie/cowrie/etc/userdb.txt
echo "#root:x:!/honeypot/i" >> /home/cowrie/cowrie/etc/userdb.txt
echo "root:x:*" >> /home/cowrie/cowrie/etc/userdb.txt
echo "tomcat:x:*" >> /home/cowrie/cowrie/etc/userdb.txt
echo "oracle:x:*" >> /home/cowrie/cowrie/etc/userdb.txt
chown cowrie:cowrie /home/cowrie/cowrie/etc/userdb.txt
cat /home/cowrie/cowrie/etc/userdb.txt
sudo systemctl restart cowrie-honeypot.service
echo ________________________________________

echo 13. Add Port 2332
echo ________________________________________
grep '#Port 22' /etc/ssh/sshd_config
sed -i 's/#Port 22/Port 2332/g' /etc/ssh/sshd_config
grep 'Port 2332' /etc/ssh/sshd_config
sudo systemctl restart sshd.service
echo ________________________________________

echo 14. Add file userdb.txt
echo ________________________________________
echo "Installation Complete"
echo REMEMBER - Restart SSH Conn. : ssh -i .ssh/..s.pem ubuntu@IP_ADDR -p PORT_NR
echo ______________________________________________
echo "Check Cowrie is running, CTRL+C to quit"
sudo systemctl status cowrie-honeypot.service
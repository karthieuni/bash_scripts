#!/bin/sh

#This program tests for internet connection
#First allow permissions to user: sudo chmod u+x internet_con.sh
#To run the automated script: sudo ./internet_con.sh

#Ping Test (Internet)
echo ________________________________________
echo Pinging to 8.8.8.8
echo ________________________________________
sudo ping -c 3 8.8.8.8

echo ________________________________________
echo Pinging to Google DNS Server
echo ________________________________________
sudo ping -c 3 google.com

echo "Internet Connection Is Established\n\n"
#!/bin/sh
#This program checks for internet connectivity and installs all the necessary updates + upgrades
#First allow permissions to user: sudo chmod u+x setup_wifi.sh
#To run the automated script: sudo ./setup_wifi.sh

SSID="TestWifi"   #Add the SSID inside the two semicolons
PASSWORD="1234"   #Add the PSK inside the two semicolons
(
echo "network={"
echo "    ssid=\"${SSID}\""
echo "    psk=\"${PASSWORD}\""
echo "}"
) | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
echo __________________________________________________
echo To edit the wifi setup go to /etc/wpa_supplicant/wpa_supplicant.conf
echo __________________________________________________


#!/bin/sh

#This program generates RSA SSH Keys and copies also copies the gitlab authorized keys from client to server
#First allow permissions to user: sudo chmod u+x ssh_client_to_server.sh
#To run the automated script: sudo ./ssh_client_to_server.sh

echo ________________________________________
echo Installing Curl to fetch the SSH keys from Gitlab
echo ________________________________________
sudo apt install curl -y
echo ________________________________________
echo Generating RSA SSH KEY
echo ________________________________________
ssh-keygen -o -t rsa -b 4096 -C "karthieuni@gmail.com" -N "" -f ~/.ssh/id_rsa
echo ________________________________________
echo Fetching SSH Authorized Keys from Gitlab
echo ________________________________________
mkdir ~/.ssh
curl https://gitlab.com/karthieuni.keys >> ~/.ssh/authorized_keys  ##ADD YOUR OWN GITLAB USER HERE
echo SSH keys set
sudo systemctl enable ssh
sudo systemctl start ssh
echo ________________________________________
echo SSH keys are now successfully added to the laptop!!!
echo ________________________________________
echo View the changes:
cat ~/.ssh/authorized_keys
echo systemctl status ssh
echo ________________________________________
echo ________________________________________
echo ________________________________________
echo Copying laptop Public Key to a Server
echo ________________________________________
ssh-copy-id -i ~/.ssh/id_rsa.pub server@192.168.1.2 &  #Remember to change the Server IP address
echo ________________________________________
echo Successfully copied Public Key to Server
echo ________________________________________
echo Test the NEW key using the Private Key
ssh -i ~/.ssh/id_rsa server@192.168.1.2
echo ________________________________________


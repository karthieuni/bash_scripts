#!/bin/sh

#This program checks for internet connectivity and installs all the necessary updates + upgrades
#First allow permissions to user: sudo chmod u+x upgrade_update.sh
#To run the automated script: sudo ./upgrade_update.sh
#For creating a log file: bash -x upgrade_update.sh 2>&1 | tee installationlog.txt

#Ping Test (Internet)
echo ________________________________________
echo Pinging to 8.8.8.8
echo ________________________________________
sudo ping -c 3 8.8.8.8

echo ________________________________________
echo Pinging to Google DNS Server
echo ________________________________________
sudo ping -c 3 google.com

echo "Internet Connection Is Established\n\n"

#Update & Upgrade the Raspberry Pi:
echo ________________________________________
echo Removing unwanted caches
sudo rm /var/lib/dpkg/lock-frontend
sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
echo ________________________________________
echo Done removing caches
echo ________________________________________
echo Starting updates and upgrades
echo ________________________________________
sudo apt -y update && sudo apt -y upgrade
echo ________________________________________
echo Successfully updated and upgraded
echo ________________________________________
echo Starting updates to the System Kernel
echo ________________________________________
sudo apt -y autoremove
sudo apt -y dist-upgrade
echo ________________________________________
echo Successfully updated the System Kernal
echo ________________________________________
echo Enabling SSH and VNC server
echo ________________________________________
sudo systemctl enable ssh
sudo systemctl start ssh
sudo systemctl start vncserver-x11-serviced.service
sudo systemctl enable vncserver-x11-serviced.service
echo ________________________________________
echo ________________________________________
echo ________________________________________
echo Everything Is Installed Successfully !!!
echo ________________________________________

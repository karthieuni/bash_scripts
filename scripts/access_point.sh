#!/bin/bash

#First allow permissions to user: sudo chmod u+x access_point.sh
#To run the automated script: sudo ./access_point.sh

#Non-modified version demo: https://www.youtube.com/watch?v=qMT-0mz1lkI
#Install autohotspot from raspberrypiconnect.com
#http://www.raspberryconnect.com/network/item/330-raspberry-pi-auto-wifi-hotspot-switch-internet
#This version will provide internet when using the hotspot IF an ethernet cable is plugged in"
#This will be useful with AREDN mesh networking

#Check if program is running as root
who=$(whoami)
if [ $who == "root" ]
then
echo ""
else
echo "Script should be run as root"
echo "sudo autohotspotN-setup"
fi

#Check if the operating system is running buster before proceeding
#And if true grab alternative script
BUSTER=$(cat /etc/os-release | grep -i buster)

if [ -z "$BUSTER" ]
then
echo ""
else
wget https://gitlab.com/karthieuni/bash_scripts/raw/master/scripts/access_point/autohotspot-buster
chmod +x autohotspot-buster
./autohotspot-buster
rm autohotspotN-setup
exit 0
fi

#Update packages
apt-get update

#Install hostapd, dnsmasq
apt-get install -y hostapd
apt-get install -y dnsmasq

#Stop both services
systemctl disable hostapd
systemctl disable dnsmasq

mkdir -p $HOME/temp

wifipass () {
echo;echo;echo
echo "Type in a password used to connect to Pi's Hotspot"
echo "Password should be between 8-63 characters"
read -p "Enter password to use with new hotspot: " wifipasswd
echo;echo
echo "You entered $wifipasswd"
read -p "Is this correct (y/n): " wifians
if [ $wifians == "y" ]
then
echo
else
wifipass
fi
}

wifipass

cd $HOME/temp

wget https://gitlab.com/karthieuni/bash_scripts/raw/master/scripts/access_point/hostapd.txt

#Set new hotspot passwd
sed -i "s/wpa_passphrase=1234567890/wpa_passphrase=$wifipasswd/" $HOME/temp/hostapd.txt
#Set country to DK
sed -i 's/country_code=GB/country_code=DK/' $HOME/temp/hostapd.txt

#Move hostapd to correct location
mv $HOME/temp/hostapd.txt /etc/hostapd/hostapd.conf

sed -i s'/#DAEMON_CONF=""/DAEMON_CONF="\/etc\/hostapd\/hostapd.conf"/' /etc/default/hostapd
sed -i s'/DAEMON_OPTS=""/#DAEMON_OPTS=""/' /etc/default/hostapd

#Add needed info to dnsmasq.conf
echo "#AutoHotspot config" >> /etc/dnsmasq.conf
echo "interface=wlan0" >> /etc/dnsmasq.conf
echo "bind-dynamic" >> /etc/dnsmasq.conf
echo "server=8.8.8.8" >> /etc/dnsmasq.conf
echo "domain-needed" >> /etc/dnsmasq.conf
echo "bogus-priv" >> /etc/dnsmasq.conf
echo "dhcp-range=192.168.4.150,192.168.4.200,255.255.255.0,12h" >> /etc/dnsmasq.conf
echo "#Set up redirect for email.com" >> /etc/dnsmasq.conf
echo "dhcp-option=3,192.168.4.10" >> /etc/dnsmasq.conf
echo "address=/email.com/192.168.4.10" >> /etc/dnsmasq.conf

mv /etc/network/interfaces /etc/network/interfaces.org

echo "source-directory /etc/network/interfaces.d" >> /etc/network/interfaces


echo "nohook wpa_supplicant" >> /etc/dhcpcd.conf

#Setup ip forward
sed 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf

cd $HOME/temp

wget https://gitlab.com/karthieuni/bash_scripts/raw/master/scripts/access_point/autohotspot-service.txt

#Create autohotspot service file
mv autohotspot-service.txt /etc/systemd/system/autohotspot.service

#Start autohotspot service
systemctl enable autohotspot.service

#Check if iw installed. install if not

iwcheck=$(dpkg --get-selections | grep -w "iw")
if [ -z "iw" ]
then
apt-get install iw
fi

#Install autohotspot script
cd $HOME/temp
wget https://gitlab.com/karthieuni/bash_scripts/raw/master/scripts/access_point/autohotspotN.txt
#mod ip address for our custom setup
sed -i 's/192.168.50.5/192.168.4.10/' autohotspotN.txt
mv autohotspotN.txt /usr/bin/autohotspotN
chmod +x /usr/bin/autohotspotN

#Shackwifi function
shackwifi1 () {
echo "Type in the SSID for that provides you with an internet connection: "
read shackwifi
echo "Type in the password (PSK): "
read shackpass

echo;echo;
echo "Check what you have typed is correct "
echo "wifi $shackwifi"
echo "passwd $shackpass"
echo "Is this correct (y/n): "
read shackans
if [ $shackans == "y" ]
then
echo
else
shackwifi1
fi
}

#Run shackwifi function
shackwifi1

#Add shack wifi to wpa_supplicant.conf
echo "network={" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "ssid=\"$shackwifi\"" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "psk=\"$shackpass\"" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "key_mgmt=WPA-PSK" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "}" >> /etc/wpa_supplicant/wpa_supplicant.conf

#Remove hostapd masked error on first run of hotspot
systemctl unmask hostapd

echo;echo;echo
echo "A reboot is required to complete the setup"
echo "Wifi/AutoHotSpot will not work until reboot"
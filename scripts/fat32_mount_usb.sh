#!/bin/sh

#This program checks for internet connectivity and installs all the necessary updates + upgrades
#First allow permissions to user: sudo chmod u+x fat32_mount_usb.sh
#To run the automated script: sudo ./fat32_mount_usb.sh
#https://pcmac.biz/raspberry-pi-mount-usb-drive.html

sudo blkid
sudo mkdir /mnt/usb
sudo mount /dev/sda1 /mnt/usb
sudo chmod 775 /mnt/usb
echo _________________________________
echo The directory to the mounted USB:
echo /mnt/usb
echo ____________________________________________
echo not there try this: /mnt/usb/media or /media
echo ____________________________________________

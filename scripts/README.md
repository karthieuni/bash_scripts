# Description of how to use the scripts #

Keep in mind that this scripts have only be tested on Raspberry Pi & 
Ubuntu operating systems.

First allow permissions: ***chmod u+x [...].sh***

Run the script: ***sudo ./[...].sh***

1. install.sh is a script that fetchs the project in a zip file and 
extracts automatically to folder. The folder can be modified for the 
user liking.

2. update_upgrade.sh is a script which updates and upgrades all the 
necessary basic setup files. 

3. ssh_client_to_server.sh is a test script because works when 
transferring the ssh authorized keys to server for SSH access.

4. push_ssh.sh is a more simplier script for pushing the SSH key pairs 
to the authorized keys file. 

5. rpi_static_ip.sh is a simple script that adds the SSID and PSK 
(password) for a WPA encryption network.

6. rpi_wifi_gui.sh is a advanced script that allows the user to look 
more into wifi specfics and has more advanced features for Wifi Setup, 
like hidden SSIDs, WPA, WEP, ifconfig and more.
 
7. rpi_static_ip.sh is a simple script that sets a static ip address 
for the eth0 interface.

8. internet_con.sh is a simple script to test the internet connection.

9. rpi_system_backup.sh is a simple script that backups the Raspberry Pi to an img file.

10. fat32_mount_usb.sh is a simple script to mount usb to the Raspberry Pi.

11. access_point.sh is a advanced script that makes a Wi-Fi hotspot to the RPi using stretch or buster raspbian operating systems. 


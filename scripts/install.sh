#!/bin/bash

#First allow permissions to user: sudo chmod u+x install.sh
#To run the automated script: sudo ./install.sh

project_link="https://gitlab.com/karthieuni/bash_scripts/-/archive/master/bash_scripts-master.zip"

#Get zip-file
echo Downloading the project and extracting the zip file:
echo _________________________________
wget -O $project_name.zip $project_link
#unzip file
unzip -o $project_name.zip
cp -a bash_scripts-master bash_scripts
sudo rm -rf bash_scripts-master
echo _________________________________
echo bash_scripts can be found in this directory:
pwd
echo _________________________________

#!/bin/sh

#This program checks for internet connectivity and installs all the necessary updates + upgrades
#First allow permissions to user: sudo chmod u+x static_ip.sh
#To run the automated script: sudo ./static_ip.sh

# Setting Static IP Address:
echo ________________________________________
echo Setting up a Static IP Address
echo ________________________________________
echo Setting up networking settings:
echo Added interface eth0
echo interface\ eth0 >> /etc/dhcpcd.conf
echo static\ ip_address=192.168.1.2 >> /etc/dhcpcd.conf   #ADD YOUR OWN IP HERE
echo Static IP set to 192.168.1.2
echo static\ routers=192.168.1.1 >> /etc/dhcpcd.conf      #ADD YOUR OWN GATEWAY HERE
echo Gateway set to 192.168.1.1
echo static\ domains_name_servers=8.8.8.8 >> /etc/dhcpcd.conf
echo DNS set to 8.8.8.8
echo Networking settings finished setting up.
echo ________________________________________
echo View the changes:
cat /etc/dhcpcd.conf
# Bash_Script

This is a project that runs an automated bash script which can automatically install all of the necessary files for a Linux Operating Based System ie. Raspberry Pi, Ubuntu, Cent OS, Kali Linux


**First Option: Download install.sh for easy installation to the Raspberry Pi:**

***wget https://gitlab.com/karthieuni/bash_scripts/raw/master/scripts/install.sh***


**Second Option: Download the entire project to the Raspberry Pi:**

***wget https://gitlab.com/karthieuni/bash_scripts/-/archive/master/bash_scripts-master.zip***

***unzip bash_scripts-master.zip***
